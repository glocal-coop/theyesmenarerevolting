<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/bones.php' );

require_once( 'library/metaboxes/metabox-functions.php' );

// USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
require_once( 'library/custom-post-type.php' );

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
// require_once( 'library/admin.php' );

/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  // let's get language support going, if you need it
  load_theme_textdomain( 'tymar', get_template_directory() . '/library/translation' );

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 640;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
add_image_size( 'thumb-300', 300, 150);
add_image_size( 'thumb-350', 350, 215, true);
add_image_size( 'thumb-square', 160, 160, true);

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
        'thumb-300' => __('300px by 150px'),
        'thumb-350' => __('350px by 215px'),
        'thumb-square' => __('160px by 160px'),
    ) );
}

/************* ENQUEUE AND REGISTER SCRIPTS AND STYLES *************/

function tymar_scripts_and_styles() {

    // Libaries JS
    // We use gulp to concatenate and minimize when package.json "production" is set to true.
    // See gulp.readme.md
    wp_register_script( 'libraries-js', get_stylesheet_directory_uri() . '/resources/js/libraries.min.js', array(), '', true );

    // App JS
    // We use gulp to concatenate and minimize when package.json "production" is set to true.
    // See gulp.readme.md
    wp_register_script( 'scripts-js', get_stylesheet_directory_uri() . '/resources/js/tymar.min.js', array(), '', true );

    // // CSS
    // // We use gulp to concatenate and minimize when package.json "production" is set to true.
    // // See gulp.readme.md
    // wp_register_style( 'styles-css', get_stylesheet_directory_uri() . '/resources/css/tymar.css', array(), '', 'all' );

    // enqueue styles and scripts
    wp_enqueue_script('libraries-js');
    wp_enqueue_script('scripts-js');
    wp_enqueue_style('styles-css');

}

add_action( 'wp_enqueue_scripts', 'tymar_scripts_and_styles' );

/************* THEME SUPPORT ********************/

// $defaults = array(
//   'default-image'          => get_template_directory_uri() . '/resources/images/logo.png',
//   'random-default'         => false,
//   'width'                  => 0,
//   'height'                 => 0,
//   'flex-height'            => false,
//   'flex-width'             => false,
//   'default-text-color'     => '',
//   'header-text'            => true,
//   'uploads'                => true,
//   'wp-head-callback'       => '',
//   'admin-head-callback'    => '',
//   'admin-preview-callback' => '',
// );
// add_theme_support( 'custom-header', $defaults );


/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'tymar' ),
		'description' => __( 'The first (primary) sidebar.', 'tymar' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

  register_sidebar(array(
    'id' => 'footer1',
    'name' => __( 'Footer 1', 'tymar' ),
    'description' => __( 'The first footer widget.', 'tymar' ),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));

  register_sidebar(array(
    'id' => 'footer2',
    'name' => __( 'Footer 2', 'tymar' ),
    'description' => __( 'The second footer widget.', 'tymar' ),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));

  register_sidebar(array(
    'id' => 'footer3',
    'name' => __( 'Footer 3', 'tymar' ),
    'description' => __( 'The third footer widget.', 'tymar' ),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));

  register_sidebar(array(
    'id' => 'footer4',
    'name' => __( 'Footer 4', 'tymar' ),
    'description' => __( 'The fourth footer widget.', 'tymar' ),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'tymar' ),
		'description' => __( 'The second (secondary) sidebar.', 'tymar' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!


/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php // custom gravatar call ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/resources/images/nothing.gif" />
        <?php // end custom gravatar call ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'tymar' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'tymar' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'tymar' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'tymar' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!


/*
This is a modification of a function found in the
twentythirteen theme where we can declare some
external fonts. If you're using Google Fonts, you
can replace these fonts, change it in your scss files
and be up and running in seconds.
*/
// function bones_fonts() {
//   wp_register_style('googleFonts', 'http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic');
//   wp_enqueue_style( 'googleFonts');
// }
//add_action('wp_print_styles', 'bones_fonts');

/************* STRIP CATEGORY DESCRIPTION FROM P TAG ********************/

remove_filter('term_description','wpautop');

/************* ALLOW SHORTCODES IN WIDGETS ********************/

add_filter('widget_text', 'do_shortcode');

/************* MAIN MENU SLUG ********************/

// Adds the page slug to the main menu items

function tymar_nav_class( $classes, $item ) {
  if( 'page' == $item->object ){
    $page = get_post( $item->object_id );
    $classes[] = $page->post_name;
  } return
  $classes;
}
add_filter( 'nav_menu_css_class', 'tymar_nav_class', 10, 2 );

/************* BODY TAG SLUG ********************/

// Adds the page slug to the body tag

function tymar_body_class( $classes ) {
  global $post;
  if ( isset( $post ) ) {
    $classes[] = $post->post_type . '-' . $post->post_name;
  }
  return $classes;
  }

add_filter( 'body_class', 'tymar_body_class' );


/************* SUBMENU ********************/


class Walker_SubNav_Menu extends Walker_Nav_Menu {
    var $target_id = false;

    function __construct($target_id = false) {
        $this->target_id = $target_id;
    }

    function walk($items, $depth) {
        $args = array_slice(func_get_args(), 2);
        $args = $args[0];
        $parent_field = $this->db_fields['parent'];
        $target_id = $this->target_id;
        $filtered_items = array();

        // if the parent is not set, set it based on the post
        if (!$target_id) {
            global $post;
            foreach ($items as $item) {
                if ($item->object_id == $post->ID) {
                    $target_id = $item->ID;
                }
            }
        }

        // if there isn't a parent, do a regular menu
        if (!$target_id) return parent::walk($items, $depth, $args);

        // get the top nav item
        $target_id = $this->top_level_id($items, $target_id);

        // only include items under the parent
        foreach ($items as $item) {
            if (!$item->$parent_field) continue;

            $item_id = $this->top_level_id($items, $item->ID);

            if ($item_id == $target_id) {
                $filtered_items[] = $item;
            }
        }

        return parent::walk($filtered_items, $depth, $args);
    }

    // gets the top level ID for an item ID
    function top_level_id($items, $item_id) {
        $parent_field = $this->db_fields['parent'];

        $parents = array();
        foreach ($items as $item) {
            if ($item->$parent_field) {
                $parents[$item->ID] = $item->$parent_field;
            }
        }

        // find the top level item
        while (array_key_exists($item_id, $parents)) {
            $item_id = $parents[$item_id];
        }

        return $item_id;
    }
}

/************* GOT ID FROM SLUG ********************/

function get_ID_by_slug($slug) {
    $page = get_page_by_path($slug);
    if ($page) return $page->ID;
    else return null;
}

//TODO: These are the same - find instances were used, remove dupe
function get_page_id_from_slug($slug) {
  $page = get_page_by_path($slug);
  if ($page) {
      return $page->ID;
  } else {
      return null;
  }
}


/************* FEATURED IMAGE PREVIEW IN ADMIN ********************/

// add_theme_support( 'post-thumbnails' ); // theme should support
function tymar_add_post_thumbnail_column( $cols ) { // add the thumb column
  // output feature thumb in the end
  //$cols['tymar_post_thumb'] = __( 'Featured image', 'tymar' );
  //return $cols;
  // output feature thumb in a different column position
  $cols_start = array_slice( $cols, 0, 2, true );
  $cols_end   = array_slice( $cols, 2, null, true );
  $custom_cols = array_merge(
    $cols_start,
    array( 'tymar_post_thumb' => __( 'Featured image', 'tymar' ) ),
    $cols_end
  );
  return $custom_cols;
}
add_filter( 'manage_posts_columns', 'tymar_add_post_thumbnail_column', 5 ); // add the thumb column to posts
add_filter( 'manage_pages_columns', 'tymar_add_post_thumbnail_column', 5 ); // add the thumb column to pages

function tymar_display_post_thumbnail_column( $col, $id ) { // output featured image thumbnail
  switch( $col ){
    case 'tymar_post_thumb':
      if( function_exists( 'the_post_thumbnail' ) ) {
        echo the_post_thumbnail( 'thumbnail' );
      } else {
        echo __( 'Not supported in theme', 'tymar' );
      }
      break;
  }
}
add_action( 'manage_posts_custom_column', 'tymar_display_post_thumbnail_column', 5, 2 ); // add the thumb to posts
add_action( 'manage_pages_custom_column', 'tymar_display_post_thumbnail_column', 5, 2 ); // add the thumb to pages



// Creating an ajax route in a way.
function tymar_ajaxload_trailer(){
  $page_id = get_page_id_from_slug("trailer");
  $content = get_post($page_id)->post_content;
  header( "Content-Type: application/json" );
  echo json_encode($content);
  exit;
}
// if both logged in and not logged in users can send this AJAX request,
// add both of these actions, otherwise add only the appropriate one
add_action( 'wp_ajax_nopriv__load_tymar_trailer', 'load_tymar_trailer' );
add_action( 'wp_ajax_load_tymar_trailer', 'load_tymar_trailer' );

// get a random background image.
function tymar_get_random_homepage_bg_url(){
  $rand = rand ( 1 , 24 );
  $backgrounds_uri_path = get_bloginfo('stylesheet_directory') . '/resources/images/backgrounds/';
  $backgrounds_server_path = get_stylesheet_directory() . '/resources/images/backgrounds/';
  $files = glob($backgrounds_server_path.'*.jpg');
  $file = array_rand($files);
  return $backgrounds_uri_path . basename($files[$file]);
}

/************* CUSTOMIZE THE PASSWORD FORM ********************/

function tymar_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    ' . __( "This content is top secret. Please enter the secret code from your decoder ring to view:" ) . '
    <label for="' . $label . '">' . __( "Secret Code:" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" /><input type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" />
    </form>
    ';
    return $o;
}
add_filter( 'the_password_form', 'tymar_password_form' );


/************* DEREGISTER CIVI CSS ********************/

if(is_plugin_active( 'civicrm' )) {

  add_action( 'wp_print_styles', 'ymar_default_civicrm_styles', 100 );

  function ymar_deregister_default_civicrm_styles() {
    // CRM-11823 - If Civi bootstrapped, then remove default styles
    global $civicrm_root;
    if (empty($civicrm_root)) {
      return;
    }
    wp_deregister_style( 'civicrm/css/civicrm.css' );
    $file = CRM_Core_Resources::singleton()->getUrl('civicrm', 'css/civicrm.css', TRUE);
    CRM_Core_Region::instance('html-header')->update($file, array('disabled' => TRUE));
  }

  add_action( 'wp_print_styles', 'ymar_register_glocal_civicrm_styles', 110 );

  function ymar_register_glocal_civicrm_styles() {
    // CRM-11823 - If Civi bootstrapped, then add custom styles
    global $civicrm_root;
    if (empty($civicrm_root)) {
      return;
    }

    wp_enqueue_style ('glocal_civicrm', get_stylesheet_directory_uri() . '/civicrm/civicrm.css' );
  }

}



/* DON'T DELETE THIS CLOSING TAG */ ?>
