<div id="video_embed" class="iframe media">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php 
			echo get_the_content(); ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>