
<?php get_template_part( 'partials/header' ); ?>

<main>

	<?php get_template_part( 'partials/media-carousel' ); ?>

	<?php get_template_part( 'partials/content-page' ); ?>
	
	<section class="post-list reviews">
		<header class="section-header">
			<h2 class="section-title" itemprop="headline">Reviews</h2>
		</header>

		<?php get_template_part( 'partials/queries/posts', 'reviews' ); ?>

		<footer>
			<a href="/press-media/reviews/" class="more">More Reviews</a>
		</footer>
	</section>
	

	<!-- <section class="post-list press">
		<header class="section-header">
			<h2 class="section-title" itemprop="headline">Press Coverage</h2>
		</header>
		
		<?php// get_template_part( 'partials/queries/posts', 'press-coverage' ); ?>

		<footer>
			<a href="/press-media/press-coverage/" class="more">More Press</a>
		</footer>
	</section> -->
	
</main>

<?php get_template_part( 'partials/footer' ); ?>
