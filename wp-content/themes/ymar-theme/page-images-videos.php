<?php get_template_part( 'partials/header' ); ?>

<main id="content" role="main">

	<section class="post-list media image-gallery">
		<?php
		$images = get_posts(array(
		    'post_type' => 'attachment',
		    'post_mime_type' => 'image',
            'posts_per_page'   => 12,
		    'tax_query' => array(
		        array(
		        'taxonomy' => 'media_cat',
		        'field' => 'name',
		        'terms' => 'Press')
		    ))
		);

		foreach ($images as $image) {
			$filename = basename(wp_get_attachment_url( $image->ID ));
		?>

		<article id="media-<?php the_ID(); ?>" <?php post_class(); ?>  role="article">
			<div class="post-body">
				<span class="post-thumbnail"><?php echo wp_get_attachment_image( $image->ID, 'thumb-350' ); ?></span>
				<p class="post-meta">
					<a href="<?php echo wp_get_attachment_url( $image->ID ); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" download="<?php echo $filename; ?>" class="download-link">Download</a>
				</p>

			</div>
		</article>


		<?php } ?>
		<?php wp_reset_postdata();?>

	</section>

</main>

<?php get_template_part( 'partials/footer' ); ?>
