<?php get_template_part( 'partials/header' ); ?>

<?php 
global $post;
$slug = get_post( $post )->post_name;
?>

<main id="content" role="main">

	<?php get_template_part( 'partials/content', 'page' ); ?>

	<?php if(is_page('partners-funders')) { ?>
	<!-- If the About > Partners & Funders page -->

	<?php
	$idObj = get_category_by_slug('funders');
	$catid = $idObj->term_id;
	?>

	<section class="post-list funders">

		<?php get_template_part( 'partials/queries/posts', 'funders' ); ?>

	</section>

	<?php } elseif(is_page('take-action')) { // If the Take Action page ?>

	<!-- If the Take Action page -->
	<!-- <section class="post-list">

		<div class="connect">
			<header class="section-header">
				<h2 class="section-title" itemprop="headline">Connect With Activist Organizations</h2>
			</header>

			<?php// get_template_part( 'partials/queries/posts', 'organizations' ); ?>

		</div>

	</section> -->

	<?php } else { ?>

	<section class="post-list <?php echo $slug; ?>">

	<?php if(is_page('about')) { ?>
	<!-- If the About page -->

		<?php get_template_part( 'partials/queries/posts', 'filmmakers' ); ?>

	<?php } ?>	

	<?php if(is_page('who-are-the-yes-men')) { ?>
	<!-- If the About > Who Are the Yes Men page -->

		<?php get_template_part( 'partials/queries/posts', 'yes-men' ); ?>

	<?php } ?>

	<?php if(is_page('characters')) { ?>
	<!-- If the About > Characters page -->

		<?php get_template_part( 'partials/queries/posts', 'characters' ); ?>

	<?php } ?>

	<?php if(is_page('reviews')) { ?>
	<!-- If the Press & Media > Reviews page -->

		<?php get_template_part( 'partials/queries/posts', 'reviews' ); ?>

	<?php } ?>

	<?php if(is_page('press-coverage')) { ?>
	<!-- If the Press & Media > Press Coverage page -->

		<?php get_template_part( 'partials/queries/posts', 'press-coverage' ); ?>

	<?php } ?>

	<?php if(is_page('secret-content')) { ?>
	<!-- If the Secret Content page -->
		<?php
		// If password has been entered, show content
		if(! post_password_required() ) {
		?>
			
			<?php get_template_part( 'partials/queries/posts', 'secret' ); ?>

		<?php  } ?>
	<?php  } ?>

	</section>

	<?php  } ?>

</main>

<?php get_template_part( 'partials/footer' ); ?>

