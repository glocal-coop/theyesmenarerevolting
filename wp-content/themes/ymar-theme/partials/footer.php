        <div id="watch-the-trailer">
            <a name="trailer"/>
            <section id='trailerpage' class='page'>
                <a href='#' class='close'>&#x2715;<span>Close</span></a>
            
                <div id="video_embed" class="iframe media">
                    <?php 
                    $page_ID = get_ID_by_slug('trailer');
                    $trailer = get_post($page_ID);
                    echo $trailer -> post_content;
                    ?>
                </div>

            </section>
        </div>

      <!--<div id="video_embed" class="iframe media">
        <iframe src="//www.youtube.com/embed/9Ln3M8xfiZ8?version=3&enablejsapi=1" allowfullscreen="" frameborder="0" height="315" width="560"></iframe>
      </div>-->

		<footer id="footer" role="contentinfo">
			<div class="inner-footer">
        <div class="footer-links">
          <h3 class="tab twitter" data-tab="#twitter">
            <a href="https://twitter.com/theyesmen" target="_blank">
              <span class="icon">&nbsp;</span>
              <span class="offscreen">Twitter</span>
              <span class="close">&#x2715;</span>
            </a>
          </h3>
          <h3 class="tab fb" data-tab="#facebook">
            <a href="https://www.facebook.com/pages/The-Yes-Men/96761506615" target="_blank">
              <span class="icon">&nbsp;</span>
              <span class="offscreen">Facebook</span>
              <span class="close">&#x2715;</span>
            </a>
          </h3>
          <h3 class="tab asb" data-tab="#asb-promo">
            <a href="http://yeslab.org/actionswitchboard/" target="_blank">
              <span class="offscreen">ASB</span>
              <span class="close">&#x2715;</span>
            </a>
          </h3>
          <section id="sign-up">
            <div class="entry">
              <div class="offscreen" id="signup-message">Enter your email to join our mailing list</div>
              <?php dynamic_sidebar( 'footer4' ); ?>
            </div>
          </section>
        </div>
				<section class="drawer" id="twitter">
					<div class="content">
            <!-- <h3>Twitter</h3> -->
            <?php dynamic_sidebar( 'footer1' ); ?>
          </div>
				</section>
				<section class="drawer" id="facebook">
					<div class="content">
            <!-- <h3>Facebook</h3> -->
            <?php dynamic_sidebar( 'footer2' ); ?>
					</div>
				</section>
				<section class="drawer" id="asb-promo">
					<div class="content">
            <!-- <h3>ASB Content</h3> -->
						<?php dynamic_sidebar( 'footer3' ); ?>
					</div>
				</section>
			</div>
		</footer>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
