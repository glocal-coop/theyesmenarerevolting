<footer class="post-footer">

<?php if(in_category( 'press-coverage' )) { ?>

	<span class="post-meta source"><a href="<?php echo get_post_meta($post->ID, 'source_link', true); ?>" target="_blank"><?php echo get_post_meta($post->ID, 'source_name', true); ?></a></span>

<?php } ?>

<?php if(in_category( 'reviews' )) { ?>

	<span class="post-meta source"><?php echo get_post_meta($post->ID, 'source_name', true); ?></span>

<?php } ?>

</footer>