<div class="post-body">

	<?php if(is_page('about')) { ?>

		<?php get_template_part( 'partials/thumbnail'); ?>

	<?php } else { ?>

	<?php get_template_part( 'partials/thumbnail', '300' ); ?>

	<?php } ?>

	<?php the_content(); ?>
</div>