<?php 
if(has_post_thumbnail()) { ?>
	<span class="post-thumbnail"><?php the_post_thumbnail('bones-thumb-600'); ?></span>
<?php } ?>