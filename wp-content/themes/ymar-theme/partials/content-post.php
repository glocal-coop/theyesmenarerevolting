<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article id="<?php echo get_post_type( get_the_ID() ); ?>-<?php the_ID(); ?>" <?php post_class(); ?> role="article">

	<?php get_template_part( 'partials/item-post-body' ); ?>

</article>

<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata(); ?>