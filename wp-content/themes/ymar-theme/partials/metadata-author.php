<p class="byline vcard">
	<?php printf( __( 'Added <time class="updated" datetime="%1$s" pubdate>%2$s</time>', 'tymar' ), get_the_time('Y-m-j'), get_the_time(get_option('date_format'))); ?>
</p>