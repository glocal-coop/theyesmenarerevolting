<article id="<?php echo get_post_type( get_the_ID() ); ?>-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		<?php get_template_part( 'partials/thumbnail', '300' ); ?>
	</a>
</article>

