<?php 
	if(is_page('about' || 'take-action' || 'screenings-events')) {
		$introclass = 'intro';
	}
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article id="<?php echo get_post_type( get_the_ID() ); ?>-<?php the_ID(); ?>" <?php post_class($introclass); ?> role="article">

	<?php if(is_page('about')) { ?>
	<!-- If the About page -->

		<?php get_template_part( 'partials/item-page', 'header' ); ?>

	<?php } ?>	

	<div class="page-body">
		<?php the_content(); ?>
	</div>

</article>

<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata(); ?>
