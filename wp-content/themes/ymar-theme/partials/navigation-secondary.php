<?php
if(function_exists('get_ID_by_slug')) {
	$aboutid = get_ID_by_slug('about');
	$pressid = get_ID_by_slug('press-media');
}
?>

<?php if (is_page($aboutid) || is_page($pressid) || $post->post_parent == $aboutid || $post->post_parent == $pressid):?>
<nav id="sub-nav" role="navigation">
  <?php wp_nav_menu(array(
  'container' => false,
  'theme_location' => 'main-nav',
  'walker' => new Walker_SubNav_Menu,
  'container_class' => 'menu',
  'menu_class' => 'nav top-nav',
  'menu_id'         => 'submenu',
  'depth' => 1,
  )); ?>
</nav>
<?php endif;?>
