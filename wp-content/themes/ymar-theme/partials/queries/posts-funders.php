<?php
$idObj = get_category_by_slug('funders');
$catid = $idObj->term_id;
?>
<?php $args = array('post_type' => 'partners', 'category' => $catid, 'posts_per_page' => '30', 'orderby' => 'title', 'order' => 'ASC' );
$reviewposts = get_posts( $args );
foreach ( $reviewposts as $post ) : setup_postdata( $post ); ?>

	<?php get_template_part( 'partials/item-funder' ); ?>

<?php endforeach; ?>
<?php wp_reset_postdata();?>