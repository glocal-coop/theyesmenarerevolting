<?php
$idObj = get_category_by_slug('filmmakers');
$catid = $idObj->term_id;
?>
<?php $args = array('post_type' => 'people', 'category' => $catid, 'posts_per_page' => '30' );
$customposts = get_posts( $args );
foreach ( $customposts as $post ) : setup_postdata( $post ); ?>

	<?php get_template_part( 'partials/item-post' ); ?>

<?php endforeach; ?>
<?php wp_reset_postdata();?>