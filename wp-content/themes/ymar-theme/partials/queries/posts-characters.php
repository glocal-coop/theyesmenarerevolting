<?php
$idObj = get_category_by_slug('characters');
$catid = $idObj->term_id;
?>
<?php $args = array('post_type' => 'people', 'category' => $catid );
$customposts = get_posts( $args );
foreach ( $customposts as $post ) : setup_postdata( $post ); ?>
  <?php get_template_part( 'partials/item-post' ); ?>

<?php endforeach; ?>
<?php wp_reset_postdata();?>