<?php $args = array('category' => 'secret-content');
$secretposts = get_posts( $args );
foreach ( $secretposts as $post ) : setup_postdata( $post ); ?>

	<?php get_template_part( 'partials/item-post' ); ?>

<?php endforeach; ?>
<?php wp_reset_postdata();?>