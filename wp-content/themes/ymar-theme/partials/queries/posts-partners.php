<?php
	$idObj = get_category_by_slug('partners');
	$catid = $idObj->term_id;
?>
<?php $args = array('post_type' => 'partners', 'category' => $catid, 'posts_per_page' => '30' );
$reviewposts = get_posts( $args );
foreach ( $reviewposts as $post ) : setup_postdata( $post ); ?>

	<?php get_template_part( 'partials/item-partner' ); ?>

<?php endforeach; ?>
<?php wp_reset_postdata();?>