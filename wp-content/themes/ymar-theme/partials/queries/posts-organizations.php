<?php
$idObj = get_category_by_slug('organizations');
$catid = $idObj->term_id;
?>
<?php $args = array( 'post_type' => 'partners', 'category' => $catid );
$orgposts = get_posts( $args );
foreach ( $orgposts as $post ) : setup_postdata( $post ); ?>

	<?php get_template_part( 'partials/item-partner' ); ?>

<?php endforeach; ?>
<?php wp_reset_postdata();?>