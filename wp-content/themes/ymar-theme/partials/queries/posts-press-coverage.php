<?php
$idObj = get_category_by_slug('press-coverage');
$catid = $idObj->term_id;
?>
<?php $args = array('post_type' => 'press', 'category' => $catid );
$pressposts = get_posts( $args );
foreach ( $pressposts as $post ) : setup_postdata( $post ); ?>

	<?php get_template_part( 'partials/item-press' ); ?>

<?php endforeach; ?>
<?php wp_reset_postdata();?>
	