<?php get_template_part( 'partials/head' ); ?>

			<header id="header" role="banner">
        <?php get_template_part( 'partials/navigation-main' ); ?>
			</header>

      <h1 id="logo">
        <a href="<?php echo home_url(); ?>" rel="nofollow">
          <span>The Yes Men are Revolting</span>
        </a>
      </h1>

      <?php get_template_part( 'partials/navigation-secondary' ); ?>
