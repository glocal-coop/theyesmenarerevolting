
<section class="image-gallery">
<?php
$images = get_posts(array(
    'post_type' => 'attachment',
    'posts_per_page' => 3,
    'post_mime_type' => 'image',
    'tax_query' => array(
        array(
        'taxonomy' => 'media_cat',
        'field' => 'name',
        'terms' => 'Press')
    ))
);

foreach ($images as $image) { ?>
	<article <?php post_class(); ?>>
		<?php echo wp_get_attachment_image( $image->ID, 'thumb-350' ); ?>
	</article>
<?php } ?>

</section>