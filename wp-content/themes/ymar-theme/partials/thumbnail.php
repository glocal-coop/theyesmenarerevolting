<?php 
if(has_post_thumbnail()) { ?>
	<span class="post-thumbnail"><?php the_post_thumbnail('thumbnail'); ?></span>
<?php } ?>