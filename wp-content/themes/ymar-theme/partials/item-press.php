<article id="<?php echo get_post_type( get_the_ID() ); ?>-<?php the_ID(); ?>" <?php post_class(); ?> role="article">
	
	<div class="post-body">
		<?php the_content(); ?>
	</div>

	<?php get_template_part( 'partials/item-press', 'meta' ); ?>

</article>