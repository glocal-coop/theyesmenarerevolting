<?php $civiurl = plugins_url('civicrm'); ?>
<script type="text/javascript">
var CRM = {"config":{"userFramework":"WordPress","resourceBase":"\/wp-content\/plugins\/civicrm\/civicrm\/","lcMessages":"en_US"}};
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/jquery-ui-1.9.0/js/jquery-ui-1.9.0.custom.min.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.autocomplete.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.menu.pack.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.chainedSelects.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.tableHeader.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.textarearesizer.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.form.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.tokeninput.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.timeentry.pack.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.mousewheel.pack.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/DataTables/media/js/jquery.dataTables.min.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.FormNavigate.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.validate.min.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.ui.datepicker.validation.pack.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.jeditable.mini.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.blockUI.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.notify.min.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/packages/jquery/plugins/jquery.redirect.min.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/js/rest.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/js/Common.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/js/jquery/jquery.crmeditable.js?r=ppKX1">
</script>
<script type="text/javascript" src="<?php echo $civiurl; ?>/civicrm/js/jquery/jquery.crmasmselect.js?r=ppKX1">
</script>
<script type="text/javascript">
CRM.url('init', '/wp-admin/admin.php?page=CiviCRM&q=civicrm/example&placeholder');
CRM.formatMoney('init', "$ 1,234.56");
</script>
