<article id="<?php echo get_post_type( get_the_ID() ); ?>-<?php the_ID(); ?>" <?php post_class(); ?> role="article">

	<?php get_template_part( 'partials/item-post-header' ); ?>

	<?php if(is_page('secret-content')) { ?>

		<?php get_template_part( 'partials/item-post-footer' ); ?>

	<?php } ?>

	<?php get_template_part( 'partials/item-post-body' ); ?>

</article>

