<?php get_template_part( 'partials/header' ); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part( 'partials/item-post' ); ?>

	<?php endwhile; ?>

		<?php bones_page_navi(); ?>

	<?php else : ?>

		<?php get_template_part( 'partials/footer' ); ?>

	<?php endif; ?>


<?php get_template_part( 'partials/footer' ); ?>
