<?php


// Flush rewrite rules for partner post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the partner type
function partner_post() { 
	// creating (registering) the partner type 
	register_post_type( 'partners', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Partners & Funders', 'tymar' ), /* This is the Title of the Group */
			'singular_name' => __( 'Partner', 'tymar' ), /* This is the individual type */
			'all_items' => __( 'All Partners', 'tymar' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'tymar' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Partner', 'tymar' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'tymar' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Partner', 'tymar' ), /* Edit Display Title */
			'new_item' => __( 'New Partner', 'tymar' ), /* New Display Title */
			'view_item' => __( 'View Partner', 'tymar' ), /* View Display Title */
			'search_items' => __( 'Search Partners', 'tymar' ), /* Search Partner Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'tymar' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'tymar' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Post type for Partners and Funding Partners', 'tymar' ), /* Partner Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 5, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-awards', /* the icon for the partner post type menu */
			'rewrite'	=> array( 'slug' => 'partner', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'partners', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'sticky'),
			'taxonomies'          => array( 'category', 'post_tag' ),
		) /* end of options */
	); /* end of register post type */
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'partner_post');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add partner categories (these act like categories)
	// register_taxonomy( 'partner_cat', 
	// 	array('partners'), /* if you change the name of register_post_type( 'partner_post', then you have to change this */
	// 	array('hierarchical' => true,     /* if this is true, it acts like categories */
	// 		'labels' => array(
	// 			'name' => __( 'Partner Categories', 'tymar' ), /* name of the partner taxonomy */
	// 			'singular_name' => __( 'Category', 'tymar' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Partner Categories', 'tymar' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Partner Categories', 'tymar' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Partner Category', 'tymar' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Partner Category:', 'tymar' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Partner Category', 'tymar' ), /* edit partner taxonomy title */
	// 			'update_item' => __( 'Update Partner Category', 'tymar' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Partner Category', 'tymar' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Partner Category Name', 'tymar' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true, 
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 		'rewrite' => array( 'slug' => 'partner-cat' ),
	// 	)
	// );
	
	// // now let's add partner tags (these act like categories)
	// register_taxonomy( 'partner_tag', 
	// 	array('partners'), /* if you change the name of register_post_type( 'partner_post', then you have to change this */
	// 	array('hierarchical' => false,    /* if this is false, it acts like tags */
	// 		'labels' => array(
	// 			'name' => __( 'Partner Tags', 'tymar' ), /* name of the partner taxonomy */
	// 			'singular_name' => __( 'Tag', 'tymar' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Partner Tags', 'tymar' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Partner Tags', 'tymar' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Partner Tag', 'tymar' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Partner Tag:', 'tymar' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Partner Tag', 'tymar' ), /* edit partner taxonomy title */
	// 			'update_item' => __( 'Update Partner Tag', 'tymar' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Partner Tag', 'tymar' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Partner Tag Name', 'tymar' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true,
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 	)
	// );
	

// let's create the function for the people type
function people_post() { 
	// creating (registering) the people type 
	register_post_type( 'people', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'People', 'tymar' ), /* This is the Title of the Group */
			'singular_name' => __( 'People', 'tymar' ), /* This is the individual type */
			'all_items' => __( 'All People', 'tymar' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'tymar' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Person', 'tymar' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'tymar' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Person', 'tymar' ), /* Edit Display Title */
			'new_item' => __( 'New Person', 'tymar' ), /* New Display Title */
			'view_item' => __( 'View Person', 'tymar' ), /* View Display Title */
			'search_items' => __( 'Search People', 'tymar' ), /* Search People Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'tymar' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'tymar' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Post type for Characters, Filmmakers and Yes Men', 'tymar' ), /* People Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 5, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-groups', /* the icon for the people post type menu */
			'rewrite'	=> array( 'slug' => 'people', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'people', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'sticky'),
			'taxonomies'          => array( 'category', 'post_tag' ),
		) /* end of options */
	); /* end of register post type */
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'people_post');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add character categories (these act like categories)
	// register_taxonomy( 'character_cat', 
	// 	array('characters'), /* if you change the name of register_post_type( 'character_post', then you have to change this */
	// 	array('hierarchical' => true,     /* if this is true, it acts like categories */
	// 		'labels' => array(
	// 			'name' => __( 'Character Categories', 'tymar' ), /* name of the character taxonomy */
	// 			'singular_name' => __( 'Category', 'tymar' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Character Categories', 'tymar' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Character Categories', 'tymar' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Character Category', 'tymar' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Character Category:', 'tymar' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Character Category', 'tymar' ), /* edit character taxonomy title */
	// 			'update_item' => __( 'Update Character Category', 'tymar' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Character Category', 'tymar' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Character Category Name', 'tymar' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true, 
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 		'rewrite' => array( 'slug' => 'character-cat' ),
	// 	)
	// );
	
	// now let's add character tags (these act like categories)
	// register_taxonomy( 'character_tag', 
	// 	array('characters'), /* if you change the name of register_post_type( 'character_post', then you have to change this */
	// 	array('hierarchical' => false,    /* if this is false, it acts like tags */
	// 		'labels' => array(
	// 			'name' => __( 'Character Tags', 'tymar' ), /* name of the character taxonomy */
	// 			'singular_name' => __( 'Tag', 'tymar' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Character Tags', 'tymar' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Character Tags', 'tymar' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Character Tag', 'tymar' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Character Tag:', 'tymar' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Character Tag', 'tymar' ), /* edit character taxonomy title */
	// 			'update_item' => __( 'Update Character Tag', 'tymar' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Character Tag', 'tymar' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Character Tag Name', 'tymar' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true,
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 	)
	// );

// // let's create the function for the filmmaker type
// function filmmaker_post() { 
// 	// creating (registering) the filmmaker type 
// 	register_post_type( 'filmmakers', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
// 		// let's now add all the options for this post type
// 		array( 'labels' => array(
// 			'name' => __( 'Filmmakers', 'tymar' ), /* This is the Title of the Group */
// 			'singular_name' => __( 'Filmmaker', 'tymar' ), /* This is the individual type */
// 			'all_items' => __( 'All Filmmakers', 'tymar' ), /* the all items menu item */
// 			'add_new' => __( 'Add New', 'tymar' ), /* The add new menu item */
// 			'add_new_item' => __( 'Add New Filmmaker', 'tymar' ), /* Add New Display Title */
// 			'edit' => __( 'Edit', 'tymar' ), /* Edit Dialog */
// 			'edit_item' => __( 'Edit Filmmaker', 'tymar' ), /* Edit Display Title */
// 			'new_item' => __( 'New Filmmaker', 'tymar' ), /* New Display Title */
// 			'view_item' => __( 'View Filmmaker', 'tymar' ), /* View Display Title */
// 			'search_items' => __( 'Search Filmmakers', 'tymar' ), /* Search Filmmaker Type Title */ 
// 			'not_found' =>  __( 'Nothing found in the Database.', 'tymar' ),  This displays if there are no entries yet  
// 			'not_found_in_trash' => __( 'Nothing found in Trash', 'tymar' ), /* This displays if there is nothing in the trash */
// 			'parent_item_colon' => ''
// 			), /* end of arrays */
// 			'description' => __( 'Post type for Filmmakers', 'tymar' ), /* Filmmaker Type Description */
// 			'public' => true,
// 			'publicly_queryable' => true,
// 			'exclude_from_search' => false,
// 			'show_ui' => true,
// 			'query_var' => true,
// 			'menu_position' => 5, /* this is what order you want it to appear in on the left hand side menu */ 
// 			'menu_icon' => 'dashicons-editor-video', /* the icon for the filmmaker post type menu */
// 			'rewrite'	=> array( 'slug' => 'filmmaker', 'with_front' => false ), /* you can specify its url slug */
// 			'has_archive' => 'filmmakers', /* you can rename the slug here */
// 			'capability_type' => 'post',
// 			'hierarchical' => false,
// 			/* the next one is important, it tells what's enabled in the post editor */
// 			'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'sticky'),
// 			'taxonomies'          => array( 'category', 'post_tag' ),
// 		) /* end of options */
// 	); /* end of register post type */
	
// }

// 	// adding the function to the Wordpress init
// 	add_action( 'init', 'filmmaker_post');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add filmmaker categories (these act like categories)
	// register_taxonomy( 'filmmaker_cat', 
	// 	array('filmmakers'), /* if you change the name of register_post_type( 'filmmaker_post', then you have to change this */
	// 	array('hierarchical' => true,     /* if this is true, it acts like categories */
	// 		'labels' => array(
	// 			'name' => __( 'Filmmaker Categories', 'tymar' ), /* name of the filmmaker taxonomy */
	// 			'singular_name' => __( 'Category', 'tymar' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Filmmaker Categories', 'tymar' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Filmmaker Categories', 'tymar' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Filmmaker Category', 'tymar' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Filmmaker Category:', 'tymar' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Filmmaker Category', 'tymar' ), /* edit filmmaker taxonomy title */
	// 			'update_item' => __( 'Update Filmmaker Category', 'tymar' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Filmmaker Category', 'tymar' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Filmmaker Category Name', 'tymar' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true, 
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 		'rewrite' => array( 'slug' => 'filmmaker-cat' ),
	// 	)
	// );
	
	// now let's add filmmaker tags (these act like categories)
	// register_taxonomy( 'filmmaker_tag', 
	// 	array('filmmakers'), /* if you change the name of register_post_type( 'filmmaker_post', then you have to change this */
	// 	array('hierarchical' => false,    /* if this is false, it acts like tags */
	// 		'labels' => array(
	// 			'name' => __( 'Filmmaker Tags', 'tymar' ), /* name of the filmmaker taxonomy */
	// 			'singular_name' => __( 'Tag', 'tymar' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Filmmaker Tags', 'tymar' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Filmmaker Tags', 'tymar' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Filmmaker Tag', 'tymar' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Filmmaker Tag:', 'tymar' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Filmmaker Tag', 'tymar' ), /* edit filmmaker taxonomy title */
	// 			'update_item' => __( 'Update Filmmaker Tag', 'tymar' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Filmmaker Tag', 'tymar' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Filmmaker Tag Name', 'tymar' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true,
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 	)
	// );

// let's create the function for the press type
function press_post() { 
	// creating (registering) the press type 
	register_post_type( 'press', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Press', 'tymar' ), /* This is the Title of the Group */
			'singular_name' => __( 'Press', 'tymar' ), /* This is the individual type */
			'all_items' => __( 'All Press', 'tymar' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'tymar' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Press', 'tymar' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'tymar' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Press', 'tymar' ), /* Edit Display Title */
			'new_item' => __( 'New Press', 'tymar' ), /* New Display Title */
			'view_item' => __( 'View Press', 'tymar' ), /* View Display Title */
			'search_items' => __( 'Search Press', 'tymar' ), /* Search Press Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'tymar' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'tymar' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Post type for Media Coverage and Reviews', 'tymar' ), /* Press Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-megaphone', /* the icon for the press post type menu */
			'rewrite'	=> array( 'slug' => 'press', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'press', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'sticky'),
			'taxonomies'          => array( 'category', 'post_tag' ),
		) /* end of options */
	); /* end of register post type */
	
}

	// adding the function to the Wordpress init
	add_action( 'init', 'press_post');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add press categories (these act like categories)
	// register_taxonomy( 'press_cat', 
	// 	array('press'), /* if you change the name of register_post_type( 'press_post', then you have to change this */
	// 	array('hierarchical' => true,     /* if this is true, it acts like categories */
	// 		'labels' => array(
	// 			'name' => __( 'Categories', 'tymar' ), /* name of the press taxonomy */
	// 			'singular_name' => __( 'Press Category', 'tymar' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Press Categories', 'tymar' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Press Categories', 'tymar' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Press Category', 'tymar' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Press Category:', 'tymar' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Press Category', 'tymar' ), /* edit press taxonomy title */
	// 			'update_item' => __( 'Update Press Category', 'tymar' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Press Category', 'tymar' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Press Category Name', 'tymar' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true, 
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 		'rewrite' => array( 'slug' => 'press-cat' ),
	// 	)
	// );
	
	// now let's add press tags (these act like categories)
	// register_taxonomy( 'press_tag', 
	// 	array('press'), /* if you change the name of register_post_type( 'press_post', then you have to change this */
	// 	array('hierarchical' => false,    /* if this is false, it acts like tags */
	// 		'labels' => array(
	// 			'name' => __( 'Press Tags', 'tymar' ), /* name of the press taxonomy */
	// 			'singular_name' => __( 'Tag', 'tymar' ), /* single taxonomy name */
	// 			'search_items' =>  __( 'Search Press Tags', 'tymar' ), /* search title for taxomony */
	// 			'all_items' => __( 'All Press Tags', 'tymar' ), /* all title for taxonomies */
	// 			'parent_item' => __( 'Parent Press Tag', 'tymar' ), /* parent title for taxonomy */
	// 			'parent_item_colon' => __( 'Parent Press Tag:', 'tymar' ), /* parent taxonomy title */
	// 			'edit_item' => __( 'Edit Press Tag', 'tymar' ), /* edit press taxonomy title */
	// 			'update_item' => __( 'Update Press Tag', 'tymar' ), /* update title for taxonomy */
	// 			'add_new_item' => __( 'Add New Press Tag', 'tymar' ), /* add new title for taxonomy */
	// 			'new_item_name' => __( 'New Press Tag Name', 'tymar' ) /* name title for taxonomy */
	// 		),
	// 		'show_admin_column' => true,
	// 		'show_ui' => true,
	// 		'query_var' => true,
	// 	)
	// );


	// now let's add media categories (these act like categories)
	register_taxonomy( 'media_cat', 
		array('attachment'), /* if you change the name of register_post_type( 'media_post', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Media Categories', 'tymar' ), /* name of the media taxonomy */
				'singular_name' => __( 'Media Category', 'tymar' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Media Categories', 'tymar' ), /* search title for taxomony */
				'all_items' => __( 'All Media Categories', 'tymar' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Media Category', 'tymar' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Media Category:', 'tymar' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Media Category', 'tymar' ), /* edit media taxonomy title */
				'update_item' => __( 'Update Media Category', 'tymar' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Media Category', 'tymar' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Media Category Name', 'tymar' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'media-cat' ),
		)
	);
	
	// now let's add media tags (these act like categories)
	register_taxonomy( 'media_tag', 
		array('attachment'), /* if you change the name of register_post_type( 'media_post', then you have to change this */
		array('hierarchical' => false,    /* if this is false, it acts like tags */
			'labels' => array(
				'name' => __( 'Media Tags', 'tymar' ), /* name of the media taxonomy */
				'singular_name' => __( 'Tag', 'tymar' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Media Tags', 'tymar' ), /* search title for taxomony */
				'all_items' => __( 'All Media Tags', 'tymar' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Media Tag', 'tymar' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Media Tag:', 'tymar' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Media Tag', 'tymar' ), /* edit media taxonomy title */
				'update_item' => __( 'Update Media Tag', 'tymar' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Media Tag', 'tymar' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Media Tag Name', 'tymar' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
		)
	);


?>
