<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category TYMAR
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'cmb_tymar_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_tymar_metaboxes( array $meta_boxes ) {

	$prefix = '';

	/**
	 * Metabox for Press posts
	 */

	$meta_boxes['contact_metabox'] = array(
		'id'         => 'source_metabox',
		'title'      => __( 'Source Information', 'tymar' ),
		'pages'      => array( 'press', ), // Post type
		'context'    => 'normal',
		'priority'   => 'core',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => __( 'Source Name', 'cmb' ),
				'desc' => __( '', 'tymar' ),
				'id'   => $prefix . 'source_name',
				'type' => 'text',
			),
			array(
				'name' => __( 'Source URL', 'tymar' ),
				'desc' => __( '', 'tymar' ),
				'id'   => $prefix . 'source_link',
				'type' => 'text_url',
			),
		)
	);

	// Add other metaboxes as needed

	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'init.php';

}
