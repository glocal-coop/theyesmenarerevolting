paths = require('./paths')

module.exports = (gulp) ->

  gulp.task('watch-styles', ['css'], ->
    gulp.watch(paths.css.src, ['css'])
  )

  gulp.task('watch-scripts', ['scripts'], ->
    gulp.watch(paths.scripts.watch, ['libraries','scripts'])
  )

  gulp.task('watch', ['watch-styles', 'watch-scripts'])
