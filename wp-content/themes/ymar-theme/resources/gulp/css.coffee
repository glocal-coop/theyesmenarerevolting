prefixer = require('gulp-autoprefixer')
csslint = require('gulp-csslint')
csscomb = require('gulp-csscomb')
cssmin = require('gulp-cssmin')
sass = require('gulp-sass')
paths = require('./paths')
rename = require('gulp-rename')
pkg = require('../package.json')

module.exports = (gulp) ->
  stats = {};
  gulp.task('css', ['clean'], ->
    return gulp.src(paths.css.src)
    .pipe(sass({
      includePaths: paths.css.inlcudes,
      sourceComments: 'map',
      # outputStyle: 'compressed',
      stats: stats,
      sourceMap: 'sass'
    }))
    .pipe(csslint(paths.css.csslintrc))
    .pipe(csslint.reporter())
    .pipe(gulp.dest(paths.css.dest))
    .pipe(prefixer({ cascade: true }))
    .pipe(csscomb())
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(paths.css.dest))
  )
