module.exports =
  css:
    src: 'scss/**/*.scss'
    dest: 'css'
    includes: [
      '../node_modules'
      '../bower_components'
    ]
    watch: 'scss/**/*.scss'
    csslintrc: 'scss/.csslintrc.json'
  scripts:
    src: 'js/src/**/*.js'
    dest:'js'
    libraries: 'js/libs/**/*.js'
    watch: 'js/src/**/*.js'

