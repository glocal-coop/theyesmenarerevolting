gulp = require('gulp')
paths = require('./paths')
del = require('del')
require('./css')(gulp)
require('./scripts')(gulp)
require('./watch')(gulp)

gulp.task('clean', (cb) ->
  del([
    paths.css.dest
  ], cb)
)

gulp.task('default',['libraries','scripts','css'])

module.exports = gulp
