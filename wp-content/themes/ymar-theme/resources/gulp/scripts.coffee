rename = require('gulp-rename')
uglify = require('gulp-uglify')
jshint = require('gulp-jshint')
pkg = require('../package.json')
paths = require('./paths')
stylish = require('jshint-stylish');
concat = require('gulp-concat');

sourcemaps = require('gulp-sourcemaps')

module.exports = (gulp) ->
  gulp.task('libraries', ->
    return gulp.src(paths.scripts.libraries)
    .pipe(concat('libraries.js'))
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(uglify())
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest(paths.scripts.dest))
  )
  gulp.task('scripts', ['libraries'], ->
    return gulp.src(paths.scripts.src)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe(rename("#{pkg.name}.js"))
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(uglify())
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest(paths.scripts.dest))
  )
