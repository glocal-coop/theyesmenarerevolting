(function( tymar, $, window, document, undefined ){

  tymar.dom = {
    // little jQuery shortcut to see if an element exists
    isElement: function(selector){
      return ($(selector).length > 0);
    }
  };

  tymar.Site = function(){
    this.initialize();
  };

  tymar.Site.prototype.options = {
    breakpoints: {
      phone: 500,
      tablet_v: 800,
      tablet_h: 1000,
      desktop: 1200
    },
    HEADER_MAX_HEIGHT: 120,
    FOOTER_HEIGHT: 46,
    IMG_WIDTH:    1674,
    IMG_HEIGHT:   618,
    IMG_SPACE:    627,
    IMG_DIST:     1052,
    FWIDTH:       1173,
    FHEIGHT:      851
  };

  tymar.Site.prototype.initialize = function(){

    var self = this;

    this.device = null;
    this.isPhone = false;
    this.isTablet = false;
    this.isDesktop = false;

    // some device detection help
    var bod = $("body"),
        win = {
          w:$(window).width(),
          h:$(window).height()
        };

    var bp = this.options.breakpoints;

    if( win.w < bp.phone && typeof window.orientation !== "undefined") {
       bod.addClass("mobile");
       this.isPhone = true;
       this.device = "phone";
    } else {
      if ( win.w < bp.tablet_h && typeof window.orientation !== "undefined" ) {
        bod.addClass("tablet");
        this.isTablet = true;
        this.device = "tablet";
      } else {
        bod.addClass("desktop");
        this.isDesktop = true;
        this.device = "desktop";
      }
    }

    // for testing
    // this.isPhone = true;

    // nav that collapses at small sizes
    this.nav = $(".nav-collapse").slicknav({
      label: ""
    });

    // set some members
    this.homewrap = tymar.dom.isElement(".homewrap")? $(".homewrap") : null;
    this.foreground = tymar.dom.isElement("#foreground")? $("#foreground") : null;
    this.content = tymar.dom.isElement(".content")? $(".content") : null;
    // some state storing
    this.activeFooterTab = null;
    this.activeFooterSection = null;
    // global footer sections
    this.footer = $("#footer");

    this.trailerLink = $(".watch-the-trailer a");
    this.trailerClose = $("#watch-the-trailer a.close");

    if($("body").hasClass("home")){
      this.trailerLink.click( function(e){
        $("html,body").animate({
          scrollTop: $("#watch-the-trailer").offset().top
        }, 1000);
        e.preventDefault();
      });
      this.trailerClose.click( function(e){
        e.preventDefault();
        self.stopVideo();
        $("html,body").animate({
          scrollTop: $("body").offset().top
        }, 1000);
      });
    } else {
      this.trailerLink.click( function(e){
        e.preventDefault();
        self.showTrailer();
      });
      this.trailerClose.click( function(e){
        e.preventDefault();
        self.stopVideo();
        self.hideTrailer();
      });
    }

    // image map
    if( tymar.dom.isElement("#figures") ) {
      // image map toolTip
      this.toolTip = $("#toolTip");
      this.toolTipMessage = $("#toolTipMessage");
      // responsive imagemap
      this.figures = $("#figures");
      this.figures.smartImagemap();
      // assign hover states
      $("area").each( function(){
        $(this).mouseenter( function(e){
          self.onImageMapHover( $(this), e );
        });
        $(this).mouseleave( function(){
          console.log("hideToolTip");
          self.hideToolTip( self );
        });
      });
      this.swapOutMobileGraphics();
    }

    this.initializeFooterTabs();

    if( $(".crm-submit-buttons").length ) {
      this.replaceSignupSubmit();
    }

    $("#email-Primary").attr("placeholder","Enter your email to join our mailing list.");


    $(window).resize( function(){
      self.onResize();
    });

    this.onResize();

  };

  tymar.Site.prototype.constructor = tymar.Site;

  tymar.Site.prototype.stopVideo = function(){
    // var div = $("#video_embed");
    var iframe = $("#video_embed iframe")[0].contentWindow;
    iframe.postMessage("{\"event\":\"command\",\"func\":\"stopVideo\",\"args\":\"\"}", "*");
  };

  tymar.Site.prototype.setFooterDrawerTargetHeight = function( aNumber ) {
    this.footerDrawerTargetHeight = aNumber;
  };

  tymar.Site.prototype.getFooterDrawerTargetHeight = function() {
    return this.footerDrawerTargetHeight;
  };

  tymar.Site.prototype.showTrailer = function(){
    console.log( "showTrailer" );
    $("#watch-the-trailer").addClass("expanded");
  };

  tymar.Site.prototype.hideTrailer = function(){
    console.log("hideTrailer");
    $("#watch-the-trailer").removeClass("expanded");
  };

  // tymar.Site.prototype.listenForEscPress = function( e ){
  //   if(e.keyCode === 27){
  //     this.hideTrailer();
  //   }
  // };

  tymar.Site.prototype.swapOutMobileGraphics = function(){

    var foregroundImage,
        foregroundMobileSrc,
        backgroundSrc;

    if( this.isPhone ){
      this.figures.remove();
      this.figures = null;
      foregroundImage = $(this.foreground.find("img"));
      foregroundMobileSrc = foregroundImage.attr("src").split(".png")[0] + ".mobile.png";
      foregroundImage.attr("src",foregroundMobileSrc);
      backgroundSrc = $("body").css("background-image").split(".jpg\")")[0]+".mobile.jpg\")";
      $("body").css("background",backgroundSrc);
      // @todo: swap out backgrounds too.
    }
  };

  tymar.Site.prototype.onResize = function() {

    var win = { w:$(window).width(), h:$(window).height() },
        percent = win.w/ this.options.IMG_WIDTH,
        backgroundHeight = percent * this.options.IMG_HEIGHT,
        bp = this.options.breakpoints,
        foregroundImage = null,
        self = this;

    if(win.w <= bp.phone) {
      // do phone stuff
      if( tymar.dom.isElement("#foreground") ){
        foregroundImage = $(this.foreground.find("img"));
        // swap out image only if prev image has actually loaded... should speed up initial page load
        foregroundImage.one("load", function() {
          foregroundImage.attr("src",foregroundImage.data("src-phone"));
        }).each(function() {
          if(this.complete) {
            $(this).load();
          }
        });
      }
      if( tymar.dom.isElement("#figures") ){
        this.figures.attr("src","");
        this.figures.css("display","none");
      }
    } else if(win.w <= bp.tablet_v){
      // do tablet vertical stuff
      if( tymar.dom.isElement("#foreground") ){
        foregroundImage = $(this.foreground.find("img"));
        foregroundImage.one("load", function() {
          foregroundImage.attr("src",foregroundImage.data("src-tablet-v"));
        }).each(function() {
          if(this.complete) {
            $(this).load();
          }
        });
      }
      if( tymar.dom.isElement("#figures") ){
        this.figures.one("load", function() {
          self.figures.attr("src",self.figures.data("src-tablet-v"));
        }).each(function() {
          if(this.complete) {
            $(this).load();
          }
        });
        this.figures.css("display","block");
      }
    } else {
      // larget images
      if( tymar.dom.isElement("#foreground") ){
        foregroundImage = $(this.foreground.find("img"));
         foregroundImage.one("load", function() {
          foregroundImage.attr("src",foregroundImage.data("src-desktop"));
        }).each(function() {
          if(this.complete) {
            $(this).load();
          }
        });
      }
      if( tymar.dom.isElement("#figures") ){
        this.figures.one("load", function() {
          self.figures.attr("src",self.figures.data("src-desktop"));
        }).each(function() {
          if(this.complete){
            $(this).load();
          }
        });
        this.figures.css("display","block");
      }
    }

    if( tymar.dom.isElement("#figures") ){
      this.hideToolTip();
      this.figures.css({
        width: win.w,
        height: backgroundHeight
      });
      this.figures.trigger("smartimagemap.update");
    }

    // homepage foreground
    if( tymar.dom.isElement("#foreground") ){
      var w = win.w,
          // ratio = w/this.options.FWIDTH,
          fgw = this.options.FWIDTH * percent,
          fgl;

      foregroundImage = this.foreground.find("img");
      if( win.h > win.w ){
        this.foreground.width(w*1.5);
        fgl = (win.w*0.5)-fgw;
      }else if(this.isPhone && win.h < win.w){
        this.foreground.width(w*0.375);
        fgl = win.w*0.5-fgw*0.5;
      }else{
        this.foreground.width( fgw );
        this.foreground.css("height","auto");
        fgl = win.w*0.5-fgw*0.5;
      }
      this.foreground.css("height","auto");
      this.foreground.css( "left", fgl+"px");
    }

    this.setFooterDrawerTargetHeight( $(window).height() * 0.75 );

  };

  tymar.Site.prototype.drawLine = function() {

  };

  tymar.Site.prototype.initializeFooterTabs = function(){
    var self = this;
    this.footer.find(".tab>a").click( function(e){
      e.preventDefault();
      self.toggleFooterDrawer($(this).parent());
    });
  };

  tymar.Site.prototype.replaceSignupSubmit = function(){
    var newSubmitButton = "<input accesskey=\"S\" name=\"_qf_Edit_next\" value=\"Save\" id=\"_qf_Edit_next\" src=\"/wp-content/themes/ymar-theme/resources/images/signup-submit.png\" alt=\"Sign up\" type=\"image\">";
    $("#footer .crm-submit-buttons").empty();
    $("#footer .crm-submit-buttons").append( newSubmitButton );
  };

  tymar.Site.prototype.toggleFooterDrawer = function(tab){

    var selector = tab.data("tab"),
        target = $(selector),
        fHeight = Math.round(this.getFooterDrawerTargetHeight()),
        drawerHeight = fHeight - this.options.FOOTER_HEIGHT;

    $("#footer .drawer").css("height",drawerHeight);

    if(!this.footer.hasClass("expanded")){
      // its not open... lets open it
      this.footer.addClass("expanded");
      tab.addClass("open");
      this.footer.css("max-height", fHeight );
      $(this.activeFooterSection).css("z-index", 50);
      this.activeFooterTab = tab;
      this.activeFooterSection = selector;
      target.css("z-index", 100);
    } else if (this.activeFooterSection == selector && this.footer.hasClass("expanded")) {
      // close the dawer
      $(this.activeFooterSection).css("z-index", 50);
      this.activeFooterSection = null;
      this.footer.css("max-height", this.options.FOOTER_HEIGHT);
      this.activeFooterTab.removeClass("open");
      this.activeFooterTab.find("a").blur();
      this.activeFooterTab = null;
      this.footer.removeClass("expanded");
    } else {
      // its already open, but another tab was clicked
      $(this.activeFooterSection).css("z-index", 50);
      target.css("z-index", 100);
      this.activeFooterTab.removeClass("open");
      this.activeFooterTab = tab;
      tab.addClass("open");
      this.activeFooterSection = selector;
    }

  };

  tymar.Site.prototype.hideToolTip = function(){
    this.toolTip.hide();
    $("#toolTipIndicator").hide();
  };

  tymar.Site.prototype.onImageMapHover = function(el){
    var msg = el.data("label"),
        // y = e.pageY,
        // x = e.pageX,
        toolTipStyle = {
          top: this.figures.offset().top,
          left: el.areaOffset(true).left,
          display: "block"
        },
        indicatorOffsetY = 0,
        msgOffsetLeft = 0;
    console.log("onImageMapHover", el.areaOffset(true), this.figures.offset());

    this.setToolTipMessage( msg );

    switch(el.data("label")){
      case "Yemeni Revolution":
        toolTipStyle.left += 20;
        break;
      case "Socrates":
        toolTipStyle.left += 20;
        break;
      case "Gustave Courbet":
        toolTipStyle.left += 20;
        break;
      case "Emiliano Zapata":
        toolTipStyle.left += 20;
        break;
      case "Harry Hay":
        toolTipStyle.left += 20;
        break;
      case "Emile Zola":
        toolTipStyle.left += 20;
        break;
      case "Aung San Suu Kyi":
        break;

      case "Ellen Johnson Sirleaf":
        toolTipStyle.left -= 30;
        break;
      case "Subcomandante Marcos":
        toolTipStyle.left += 20;
        msgOffsetLeft = 50;
        break;
      case "Vladimir Mayakovsky":
        toolTipStyle.left += 20;
        break;
      case "Thomas Paine":
        toolTipStyle.left += 20;
        break;
      case "Harvey Milk":
        toolTipStyle.left -= 20;
        break;
      case "Peter Staley":
        toolTipStyle.left -= 20;
        break;
      case "Desmond Tutu":
        toolTipStyle.left += 20;
        break;
      case "Larry Kramer":
        toolTipStyle.left += 20;
        break;
      case "Chen Guangcheng":
        toolTipStyle.left += 20;
        break;
      case "French Revolution":
        toolTipStyle.left += 20;
        break;
      case "Lesbian Avengers":
        toolTipStyle.left += 20;
        break;
      case "Pussy Riot":
        toolTipStyle.left += 10;
        break;
      case "Arab Spring":
        toolTipStyle.left += 20;
        break;
      case "American Revolution":
        toolTipStyle.left += 20;
        break;
      case "Suffragette":
        toolTipStyle.left += 20;
        break;
      case "ACT UP":
        indicatorOffsetY = 45;
        break;
      case "Black Panther Party":
        toolTipStyle.left += 20;
        break;
      case "Stonewall Riots":
        toolTipStyle.left += 20;
        break;
      case "Occupy Wall Street":
        break;
      case "Jose Sarria":
        toolTipStyle.left += 20;
        break;
      case "American Indian Movement":
        indicatorOffsetY = 35;
        break;
      case "Eight Hour Day":
        toolTipStyle.left += 20;
        break;
      case "Wounded Knee Incident":
        toolTipStyle.left += 20;
        break;
      case "Angela Davis":
        toolTipStyle.left += 10;
        msgOffsetLeft = 30;
        break;


    }
    this.toolTip.show();
    this.toolTip.css( toolTipStyle );
    $("#toolTipIndicator").css({
      left: this.toolTip.css("left"),
      top: toolTipStyle.top + this.toolTipMessage.height(),
      width: "1px",
      display: "block",
      height: el.areaOffset().top - this.figures.areaOffset().top + indicatorOffsetY
    });
    $("#toolTipMessage").css("margin-left",msgOffsetLeft);
  };

  tymar.Site.prototype.setToolTipMessage = function(msgString){
    this.toolTipMessage.html(msgString);
  };

})( window.tymar = window.tymar || {}, jQuery, window, document, undefined );

(function(){
  jQuery(document).ready( function() {
    new tymar.Site();
  });
})();
